# frozen_string_literal: true

Rails.application.routes.draw do
  root 'static_pages#home'
  get 'menu', to: 'static_pages#menu'
  get 'about', to: 'static_pages#about'
  get 'reminders', to: 'static_pages#reminders'
  get 'scores/categories/:id', to: 'scores#categories'
  get 'scores', to: 'scores#index'
  get 'scores/:id', to: 'scores#show'
  get 'log_out' => 'sessions#destroy', :as => 'log_out'
  get 'log_in' => 'sessions#new', :as => 'log_in'
  post 'log_in' => 'sessions#create'
  get 'sign_up' => 'users#new', :as => 'sign_up'
  post 'sign_up' => 'users#create'
  get 'categories', to: 'categories#index'
  get 'categories/:id', to: 'categories#show'
  get 'questions/:id', to: 'questions#show'
  get 'results/:id', to: 'users_answers#results'
  get 'users_answers/:id', to: 'users_answers#show'
  post 'users_answers', to: 'users_answers#create'
end
