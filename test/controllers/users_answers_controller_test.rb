require 'test_helper'

class UsersAnswersControllerTest < ActionDispatch::IntegrationTest
  test "should get create" do
    get users_answers_create_url
    assert_response :success
  end

  test "should get show" do
    get users_answers_show_url
    assert_response :success
  end

end
