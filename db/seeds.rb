# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'

def insert_init_data
  category_id = 0
  question_id = 0
  
  CSV.foreach(Rails.root.join('app/assets/csv/initial.csv'), row_sep: "\r\n", :headers => true) do |row|
    if row['category']
      category_id += 1
      Category.create(
        id: category_id,
        title: row['category'],
        description: row['description']
      )
    end
    if row['question']
      question_id += 1
      Question.create(
        id: question_id,
        category_id: category_id,
        content: row['question']
      )
    end
    Choice.create(
      question_id: question_id,
      content: row['choice'],
      correct: row['correct'] == '1' ? true : false
    )
  end
end
# categories / questions / choices
insert_init_data unless Category.first
# users / user_answers / scores

# User with no answer
User.create(email: 'no_answer@sunnyeeegame.com', password: 'password')

# User with answers of category 1
user = User.create(email: 'answered@sunnyeeegame.com', password: 'password')
category = Category.first
score = Score.create(user_id: user.id, category_id: category.id)

category.questions.each do |question|
  correct_choice = question.choices.find_by(correct: true)
  UserAnswer.create(score_id: score.id, question_id: question.id, choice_id: correct_choice.id)
end