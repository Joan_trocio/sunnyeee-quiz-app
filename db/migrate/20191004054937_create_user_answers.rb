class CreateUserAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :user_answers do |t|
      t.integer :score_id
      t.integer :question_id
      t.integer :choice_id

      t.timestamps
    end
  end
end
