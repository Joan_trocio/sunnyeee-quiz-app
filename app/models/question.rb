# frozen_string_literal: true

class Question < ApplicationRecord
  belongs_to :category
  has_many :choices
  has_many :user_answers

  def next
    category.questions.where('id > ?', id).first
  end

  def is_last?
    category.questions.last.id == id
  end

  def id_in_category
    category.questions.where('? >= id', id).count
  end
end
