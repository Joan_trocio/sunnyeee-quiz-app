# frozen_string_literal: true

class UserAnswer < ApplicationRecord
  belongs_to :score
  belongs_to :choice
  belongs_to :question

  def self.current_users_answer(score_id, question_id)
    UserAnswer.find_by(score_id: score_id, question_id: question_id)
  end
end
