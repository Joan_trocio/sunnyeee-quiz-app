# frozen_string_literal: true

class Score < ApplicationRecord
  has_many :user_answers
  belongs_to :category
  belongs_to :user

  def self.current_score(user_id, category_id)
    is_exists = Score.where(user_id: user_id, category_id: category_id).exists?
    unless is_exists
      return Score.create(user_id: user_id, category_id: category_id)
    end

    Score.find_by(user_id: user_id, category_id: category_id)
  end

  def self.exists?(user_id, category_id)
    Score.where(user_id: user_id, category_id: category_id).exists?
  end

  def passed
    corrects_count >= 16
  end

  def corrects_count
    user_answers.select do |answer|
      answer.choice.correct
    end.count
  end
end
