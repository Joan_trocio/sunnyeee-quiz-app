# frozen_string_literal: true

class Category < ApplicationRecord
  has_many :questions
  has_many :scores

  def active(user_id)
    return true if self == Category.first || Score.exists?(user_id, id)

    prev_score = Score.find_by("category_id = #{prev.id} and user_id = #{user_id}")
    prev_score&.passed
  end

  def prev
    Category.where("id < #{id}").last
  end
end
