class Choice < ApplicationRecord
  belongs_to :question
  has_many :users_answers
end
