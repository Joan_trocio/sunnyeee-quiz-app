class ScoresController < ApplicationController
  before_action :logged_in_user


  def index
    @categories = Category.all
  end

  def show
    @questions_count = Category.find(params[:id]).questions.count
    @score = Score.current_score(session[:user_id], params[:id])
  end

  def categories
    score_list = Category.find(params[:id]).scores.map do |score|
      { 
        score: score.user_answers.select { |answer| answer.choice.correct }.count,
        user_mail: score.user.email
      } 
    end
    @scores = score_list.sort_by{ |s| -s[:score] }.take(10)
  end
end
