class QuestionsController < ApplicationController
  before_action :logged_in_user

  def show
    @question = Question.find(params[:id])
  end
end
