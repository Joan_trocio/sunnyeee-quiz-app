class StaticPagesController < ApplicationController
  before_action :logged_in_user, only: [:menu, :about, :reminders]
  layout "index", only: [:about, :menu, :reminders]

  def home
  end

  def menu
  end

  def about
  end

  def reminders
  end
end
