class CategoriesController < ApplicationController
  before_action :logged_in_user
  layout "index", only: [:index, :show]

  def index
    @categories = Category.all
  end

  def show
    @category = Category.find(params[:id])
    @question = @category.questions.first
  end
end
