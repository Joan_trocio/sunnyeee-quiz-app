class SessionsController < ApplicationController
  before_action :logged_in_user, only: [:destroy]

  def new
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to reminders_url, notice: "Logged in!"
    else
      flash.now[:login_alert] = "Email or password is invalid!"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Logged out!"
  end
end
