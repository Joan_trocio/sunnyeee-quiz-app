# frozen_string_literal: true

class UsersAnswersController < ApplicationController
  before_action :logged_in_user

  def create
    question = Question.find(params[:question_id])
    @score = Score.current_score(session[:user_id], question.category_id)
    @answer = UserAnswer.find_or_initialize_by(score_id: @score.id, question_id: params[:question_id])
    @answer.choice_id = params[:choice_id]
    if @answer.save
      redirect_to action: :show, id: @answer.id
    else
      flash[:question_alert] = 'Error while sending answers!'
      redirect_to controller: 'questions', action: 'show', id: question.id
    end
  end

  def show
    @answer = UserAnswer.find(params[:id])
  end

  def results
    @category = Category.find(params[:id])
    @score = Score.find_by(user_id: current_user, category_id: @category.id)
  end
end
