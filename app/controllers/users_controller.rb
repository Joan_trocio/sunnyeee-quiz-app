class UsersController < ApplicationController
  
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      flash[:success] = "Welcome to the Sunnyee Quiz game!"
      session[:user_id] = @user.id
      redirect_to reminders_path
    else
      render "new"
    end
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end